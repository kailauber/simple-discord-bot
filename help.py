import os
import logging
from dotenv import load_dotenv
import discord
import asyncio
from discord.ext import commands, menus

class InteractiveHelpPaginator(menus.ListPageSource):
    def __init__(self, data, cog):
        super().__init__(data, per_page=5)
        self.cog = cog

    async def format_page(self, menu, entries):
        embed = discord.Embed(title=f"{self.cog.qualified_name} Commands", color=discord.Color.blurple())
        embed.set_footer(text=f"Page {menu.current_page + 1}/{self.get_max_pages()}")

        for entry in entries:
            if not entry.hidden:
                embed.add_field(name=entry.name, value=entry.short_doc or "No description available.", inline=False)

        return embed


class CustomHelpCommand(commands.HelpCommand):
    def get_cog_emoji(self, cog_name):
        # Define the emojis to use for each cog, you can use custom emojis if you want
        cog_emojis = {
            "Moderation": "🛠️",
            "Utility": "🔧",
            "Fun": "🎉",
            "Verification": "✅",
            "Audio": "🎵",
            "Money": "💰",
            "NSFW": "🔞",
            "Greeting": "👋"
        }
        return cog_emojis.get(cog_name, "❓")

    async def send_cog_help(self, cog):
        filtered = await self.filter_commands(cog.get_commands(), sort=True)
        pages = menus.MenuPages(source=InteractiveHelpPaginator(filtered, cog), clear_reactions_after=True)
        await pages.start(self.context)

    async def send_bot_help(self, mapping):
        initial_embed = discord.Embed(title='Command List', color=discord.Color.blurple())

        for cog, command_list in mapping.items():
            if cog is not None and cog.qualified_name != 'Database':  # Add this condition to skip the Database cog
                filtered = await self.filter_commands(command_list, sort=True)
                if filtered:
                    initial_embed.add_field(
                        name=f"{self.get_cog_emoji(cog.qualified_name)} {cog.qualified_name}",
                        value="",
                        inline=False
                    )

        bot_help_message = await self.context.send(embed=initial_embed)

        for cog, _ in mapping.items():
            if cog is not None and cog.qualified_name != 'Database':  # Add this condition to skip the Database cog
                await bot_help_message.add_reaction(self.get_cog_emoji(cog.qualified_name))

        # Wait for the reactions to be added before continuing with the loop
        await asyncio.sleep(1)

        def check_reaction(reaction, user):
            return user == self.context.author and str(reaction.emoji) in [self.get_cog_emoji(cog.qualified_name) for cog, _ in mapping.items() if cog is not None and cog.qualified_name != 'Database']

        while True:
            try:
                reaction, user = await self.context.bot.wait_for('reaction_add', timeout=60.0, check=check_reaction)
            except asyncio.TimeoutError:
                break
            except Exception as e:
                logging.error(f"Error handling reaction: {e}")
                await self.context.send("There was an error processing your request. Please try again later.")
            else:
                await bot_help_message.remove_reaction(reaction, user)
                for cog, _ in mapping.items():
                    if cog is not None and self.get_cog_emoji(cog.qualified_name) == str(reaction.emoji) and cog.qualified_name != 'Database':
                        await self.send_cog_help(cog)
                        break
