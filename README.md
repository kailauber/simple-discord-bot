# Simple Discord Bot

This is a Discord bot that has two cogs: `moderation.py` and `utility.py`. The `moderation.py` cog has commands for warning, kicking, banning, and muting members. The `utility.py` cog has a command for checking the bot's latency and repeating a message.

## Prerequisites

Before running the bot, you will need to have the following:

- Python 3.7+
- [Discord.py](https://discordpy.readthedocs.io/en/stable/index.html) package
- [dotenv](https://pypi.org/project/python-dotenv/) package

## Installation

1. Clone or download this repository.
2. Create a new file called `.env` in the root directory and add the following line: `BOT_TOKEN=<your-bot-token>`. Replace `<your-bot-token>` with your actual bot token.
3. Start the bot by running `python bot.py`.

## Usage

The bot has the following commands:

### Moderation Cog

- `!warn <@member> <reason>` - Sends a warning message to a member.
- `!kick <@member> <reason>` - Kicks a member from the server.
- `!ban <@member> <reason>` - Bans a member from the server.
- `!mute <@member> <reason>` - Mutes a member in the server and gives a warning.
- `!clearwarn <@member> <index_or_all>` - Clears a specific warning of a member by its index or all warnings.
- `!warnings <@member>` - Displays all warnings a user has. If no member is provided, shows your own warnings.

### Utility Cog

- `!ping` - Responds with the bot's latency in milliseconds.
- `!say <message>` - Repeats a message.

To use the bot, invite it to your Discord server and type any of the commands in a text channel that the bot has access to.

## License

This project is licensed under the MIT License.
