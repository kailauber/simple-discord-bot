import os
import logging
from dotenv import load_dotenv
import discord
import asyncio
from discord.ext import commands, menus
from cogs.moderation import Moderation
from cogs.utility import Utility
from cogs.database import Database
from cogs.fun import Fun
from cogs.verification import Verification
from cogs.audio import Audio
from cogs.nsfw import NSFW
from cogs.greeting import Greeting
from cogs.money import Money
from help import CustomHelpCommand

# Load environment variables
load_dotenv()

logging.basicConfig(level=logging.INFO)

# Set up the bot
intents = discord.Intents.all()
prefixes = {}

async def get_prefix(bot, message):
    guild = message.guild
    if guild:
        # Use the cog's get_prefix method
        prefix = await bot.get_cog('Database').get_prefix(guild.id)
        return prefix
    else:
        return '!'  # Use the default prefix in direct messages


client = commands.Bot(command_prefix=get_prefix, intents=intents)


# Event to check if a command exists and print it
@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.CommandNotFound):
        await ctx.send(f"The command `{ctx.message.content.split()[0]}` does not exist. Type `!help` to see available commands.")


async def register_cogs_async():
    """Register cogs for the bot asynchronously."""
    await client.add_cog(Utility(client))
    await client.add_cog(Database(client))
    await client.add_cog(Moderation(client))
    await client.add_cog(Fun(client))
    await client.add_cog(Verification(client))
    await client.add_cog(Audio(client))
    await client.add_cog(NSFW(client))
    await client.add_cog(Greeting(client))
    await client.add_cog(Money(client))



def set_custom_help_command():
    """Set the custom help command for the bot."""
    client.help_command = CustomHelpCommand()

@client.event
async def on_ready():
    await register_cogs_async()


def run_bot():
    """Run the bot using the BOT_TOKEN environment variable."""
    try:
        client.run(os.getenv("BOT_TOKEN"))
    except discord.errors.LoginFailure:
        logging.error("Failed to log in. Check your BOT_TOKEN.")


if __name__ == "__main__":
    set_custom_help_command()
    run_bot()
