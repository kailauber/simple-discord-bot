import discord
import random
import requests
from discord.ext import commands
import logging

logging.basicConfig(level=logging.INFO)

class NSFW(commands.Cog):
    """
    A Cog that contains NSFW commands
    """

    def __init__(self, client):
        self.client = client
        logging.info("NSFW Cog initialized")

    @commands.command(name="waifu")
    @commands.is_nsfw()
    async def waifu(self, ctx):
        """
        Returns a NSFW waifu image from waifu.pics
        """
        response = requests.get("https://api.waifu.pics/nsfw/waifu")
        image_url = response.json()["url"]
        await ctx.send(image_url)

    @commands.command(name="neko")
    @commands.is_nsfw()
    async def neko(self, ctx):
        """
        Returns a NSFW neko image from waifu.pics
        """
        response = requests.get("https://api.waifu.pics/nsfw/neko")
        image_url = response.json()["url"]
        await ctx.send(image_url)

    @commands.command(name="trap")
    @commands.is_nsfw()
    async def trap(self, ctx):
        """
        Returns a NSFW trap image from waifu.pics
        """
        response = requests.get("https://api.waifu.pics/nsfw/trap")
        image_url = response.json()["url"]
        await ctx.send(image_url)

    @commands.command(name="facepalm")
    @commands.is_nsfw()
    async def facepalm(self, ctx):
        """
        Returns a NSFW facepalm gif from nekos.best
        """
        response = requests.get("https://nekos.best/api/v2/facepalm")
        data = response.json()
        
        # Access the URL
        image_url = data["results"][0]["url"]

        await ctx.send(image_url)

    @commands.command(name="blowjob")
    @commands.is_nsfw()
    async def blowjob(self, ctx):
        """
        Returns a NSFW blowjob image from waifu.pics
        """
        response = requests.get("https://api.waifu.pics/nsfw/blowjob")
        image_url = response.json()["url"]
        await ctx.send(image_url)

async def setup(bot):
    await bot.add_cog(NSFW(bot))
