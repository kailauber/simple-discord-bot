import discord
from discord.ext import commands
import os
import logging
import asyncio

class Moderation(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.db = None
        client.loop.create_task(self.initialize_database())

    async def initialize_database(self):
        await self.client.wait_until_ready()
        self.db = self.client.get_cog('Database')
        logging.info("Moderation Cog initialized")

    async def cog_check(self, ctx):
        if self.db is None:
            await ctx.send("Database cog is not loaded.")
            return False
        return True

    async def get_member(self, ctx, arg):
        if not arg:
            return ctx.author
        try:
            return await commands.MemberConverter().convert(ctx, arg)
        except commands.MemberNotFound:
            await ctx.send("Member not found. Please provide a valid member.")
            return None
        

    @commands.has_permissions(administrator=True)
    @commands.command()
    async def changeprefix(self, ctx, new_prefix):
        """Change the bot's command prefix for this server."""
        try:
            await self.client.get_cog('Database').set_prefix(ctx.guild.id, new_prefix)
            await ctx.send(f'Prefix set to {new_prefix}')
        except Exception as e:
            await ctx.send(f'Failed to change prefix. Error: {e}')


    # Kick command
    @commands.command()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.Member = None, *, reason=None):
        """Kicks a member from the server."""
        if member is None:
            await ctx.send('Please mention a member to kick.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the kick.')
            return
        try:
            await member.kick(reason=reason)
            await ctx.send(f'{member.mention} has been kicked for {reason}.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to kick members.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to kick the member.')

    # Ban command
    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.Member = None, *, reason=None):
        """Bans a member from the server."""
        if member is None:
            await ctx.send('Please mention a member to ban.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the ban.')
            return
        try:
            await member.ban(reason=reason)
            await ctx.send(f'{member.mention} has been banned for {reason}.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to ban members.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to ban the member.')

    # Unban command
    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def unban(self, ctx, *, user_id: int):
        """Unbans a user from the server by their user ID."""
        try:
            user = await self.client.fetch_user(user_id)
            await ctx.guild.unban(user)
            await ctx.send(f'{user} has been unbanned.')
        except discord.NotFound:
            await ctx.send(f'User with ID {user_id} not found.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to unban members.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to unban the user.')

    # Mute command
    @commands.command()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def mute(self, ctx, member: discord.Member = None, *, reason=None):
        """Mutes a member in the server and gives a warning."""
        if member is None:
            await ctx.send('Please mention a member to mute.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the mute.')
            return
        try:
            # Find the "Muted" role or create it if it doesn't exist
            muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
            if muted_role is None:
                muted_role = await ctx.guild.create_role(name="Muted")

            # Assign the "Muted" role to the member
            await member.add_roles(muted_role)

            # Give the member a warning for the mute
            await self.db.add_warning(member.id, member.name, reason)
            await ctx.send(f'{member.mention} has been muted for {reason}.')

            # Add permissions to the "Muted" role to restrict the member
            for channel in ctx.guild.channels:
                await channel.set_permissions(muted_role, send_messages=False, add_reactions=False)
        except discord.Forbidden:
            await ctx.send('I do not have permission to manage roles or channels.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to mute the member.')

    # Unmute command
    @commands.command()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def unmute(self, ctx, member: discord.Member = None):
        """Unmutes a member in the server."""
        if member is None:
            await ctx.send('Please mention a member to unmute.')
            return

        muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
        if muted_role is None or muted_role not in member.roles:
            await ctx.send(f'{member.mention} is not muted.')
            return

        try:
            await member.remove_roles(muted_role)
            await ctx.send(f'{member.mention} has been unmuted.')
        except discord.Forbidden:
            await ctx.send('I do not have permission to manage roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to unmute the member.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def tempmute(self, ctx, member: discord.Member = None, duration: int = 0, *, reason=None):
        """Temporarily mutes a user for a specified duration."""
        if member is None:
            await ctx.send('Please mention a member to mute.')
            return
        if duration < 1:
            await ctx.send('Please provide a valid duration (in seconds) for the mute.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the mute.')
            return

        muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
        if muted_role is None:
            muted_role = await ctx.guild.create_role(name="Muted")

        await member.add_roles(muted_role)
        await ctx.send(f'{member.mention} has been temporarily muted for {duration} seconds due to: {reason}')

        await asyncio.sleep(duration)

        await member.remove_roles(muted_role)
        await ctx.send(f'{member.mention} has been unmuted.')

    @commands.command()
    @commands.has_permissions(manage_channels=True)
    async def slowmode(self, ctx, seconds: int):
        """Enables slow mode for a specific text channel."""
        await ctx.channel.edit(slowmode_delay=seconds)
        await ctx.send(f'Slow mode has been set to {seconds} seconds in {ctx.channel.mention}.')



    # Warn command
    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def warn(self, ctx, member: discord.Member = None, *, reason=None):
        """Sends a warning message to a member."""
        if member is None:
            await ctx.send('Please mention a member to warn.')
            return
        if reason is None:
            await ctx.send('Please provide a reason for the warning.')
            return

        if self.db is None:
            await ctx.send("Database connection is not established.")
            return

        try:
            await self.db.warnings.add_warning(member.id, member.name, reason)
            await ctx.send(f'{member.mention}, you have been warned for {reason}.')
        except Exception as e:
            await ctx.send(f"An error occurred while adding the warning: {str(e)}")
            logging.error(f"Error while adding warning: {str(e)}")


    # Clearwarn command
    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clearwarn(self, ctx, member: discord.Member = None, index_or_all: str = None):
        """Clears a specific warning of a member by its index or all warnings."""
        if member is None:
            await ctx.send('Please mention a member.')
            return

        warnings = await self.db.warnings.get_warnings(member.id)

        if not warnings:
            await ctx.send(f'{member.mention} has no warnings.')
            return
        if index_or_all == "all":
            await self.db.warnings.clear_warnings(member.id)
            await ctx.send(f"All warnings for {member.mention} have been cleared.")
        elif index_or_all is not None and index_or_all.isdigit():
            warning_index = int(index_or_all)
            if warning_index < 1 or warning_index > len(warnings):
                await ctx.send('Invalid warning index. Please provide a valid index number.')
                return

            # Remove the warning from the list
            removed_warning = warnings.pop(warning_index - 1)
            await self.db.warnings.update_warnings(member.id, warnings)
            await ctx.send(f"Warning {warning_index} for {member.mention} has been removed: '{removed_warning}'")
        else:
            await ctx.send("Please provide a valid index number or 'all' to clear all warnings.")

    # Warnings command
    @commands.command()
    async def warnings(self, ctx, *, arg=None):
        """Displays all warnings a user has. If no member is provided, shows your own warnings."""

        db_cog = self.client.get_cog("Database")
        if not db_cog:
            await ctx.send("Database cog not found.")
            return

        if arg == "all" and ctx.author.guild_permissions.administrator:
            all_warnings = []
            for member in ctx.guild.members:
                warnings = await db_cog.warnings.get_warnings(member.id)
                if warnings:
                    warning_count = len(warnings)
                    warning_list = "\n".join(f"{i+1}. {warning}" for i, warning in enumerate(warnings))
                    all_warnings.append(f'{member.mention} has {warning_count} warning(s):\n{warning_list}')
            if all_warnings:
                await ctx.send("\n\n".join(all_warnings))
            else:
                await ctx.send("No warnings found for any member.")
            return
        if arg is None:
            member = ctx.author
        else:
            try:
                member = await commands.MemberConverter().convert(ctx, arg)
            except commands.MemberNotFound:
                await ctx.send('Member not found. Please provide a valid member or use "all" to show warnings for all members.')
                return

        warnings = await db_cog.warnings.get_warnings(member.id)
        if not warnings:
            await ctx.send(f'{member.mention} has no warnings.')
        else:
            warning_count = len(warnings)
            if warning_count == 0:
                await ctx.send(f'{member.mention} has no warnings.')
            else:
                warning_list = "\n".join(f"{i+1}. {warning}" for i, warning in enumerate(warnings))
                await ctx.send(f'{member.mention} has {warning_count} warning(s):\n{warning_list}')

    @commands.command()
    async def roleinfo(self, ctx, *, role: discord.Role = None):
        """Shows information about a specific role."""
        if role is None:
            await ctx.send('Please mention a role to display information.')
            return

        embed = discord.Embed(title=f"{role.name}", color=role.color)
        embed.add_field(name="ID", value=role.id)
        embed.add_field(name="Color", value=str(role.color))
        embed.add_field(name="Position", value=role.position)
        embed.add_field(name="Mentionable", value=role.mentionable)
        embed.add_field(name="Hoisted", value=role.hoist)
        embed.add_field(name="Created at", value=role.created_at.strftime('%Y-%m-%d %H:%M:%S'))
        embed.add_field(name="Permissions", value=", ".join(permission for permission, value in role.permissions if value))

        await ctx.send(embed=embed)

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def addrole(self, ctx, member: discord.Member = None, *, role: discord.Role = None):
        """Adds a role to a user."""
        if member is None:
            await ctx.send('Please mention a member to give a role.')
            return
        if role is None:
            await ctx.send('Please mention a role to add.')
            return

        try:
            await member.add_roles(role)
            await ctx.send(f"{member.mention} has been given the {role.name} role.")
        except discord.Forbidden:
            await ctx.send('I do not have permission to manage roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to add the role to the member.')


    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def removerole(self, ctx, member: discord.Member = None, *, role: discord.Role = None):
        """Removes a role from a user."""
        if member is None:
            await ctx.send('Please mention a member to remove a role.')
            return
        if role is None:
            await ctx.send('Please mention a role to remove.')
            return

        try:
            await member.remove_roles(role)
            await ctx.send(f"{role.name} role has been removed from {member.mention}.")
        except discord.Forbidden:
            await ctx.send('I do not have permission to manage roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to remove the role from the member.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def createrole(self, ctx, *, role_name=None):
        """Creates a new role with specified permissions."""
        if role_name is None:
            await ctx.send('Please provide a name for the new role.')
            return

        try:
            new_role = await ctx.guild.create_role(name=role_name)
            await ctx.send(f"New role '{new_role.name}' has been created.")
        except discord.Forbidden:
            await ctx.send('I do not have permission to create roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to create the new role.')

    @commands.command()
    @commands.has_permissions(manage_roles=True)
    async def deleterole(self, ctx, *, role: discord.Role = None):
        """Deletes a specified role."""
        if role is None:
            await ctx.send('Please mention a role to delete.')
            return

        try:
            await role.delete()
            await ctx.send(f"Role '{role.name}' has been deleted.")
        except discord.Forbidden:
            await ctx.send('I do not have permission to delete roles.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to delete the role.')


    async def get_num_messages(self, ctx, num_messages: str):
        if num_messages == "all":
            return float("inf")
        elif num_messages.isdigit():
            num_messages = int(num_messages)
            if num_messages < 1:
                await ctx.send("Invalid number of messages. Please provide a positive number.")
                return None
            return num_messages
        else:
            await ctx.send("Invalid input. Please provide a number or 'all' for the number of messages to clear.")
            return None

async def setup(bot):
    await bot.add_cog(Moderation(bot))
