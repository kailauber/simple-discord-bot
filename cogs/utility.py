import discord
from discord.ext import commands
import logging

logging.basicConfig(level=logging.INFO)


class Utility(commands.Cog):
    """
    A Cog that provides utility commands for the bot.
    """

    def __init__(self, client):
        self.client = client
        logging.info("Utility Cog initialized")

    @commands.command()
    async def ping(self, ctx):
        """Responds with the bot's latency in milliseconds."""
        async with ctx.typing():
            latency = round(self.client.latency * 1000)
            await ctx.send(f'Pong! {latency}ms')

    @commands.command()
    @commands.has_permissions(manage_nicknames=True)
    async def nick(self, ctx, member: discord.Member = None, *, new_nickname=None):
        """Changes a user's nickname."""
        if member is None:
            await ctx.send('Please mention a member to change their nickname.')
            return
        if new_nickname is None:
            await ctx.send('Please provide a new nickname for the member.')
            return

        try:
            await member.edit(nick=new_nickname)
            await ctx.send(f"{member.mention}'s nickname has been changed to {new_nickname}.")
        except discord.Forbidden:
            await ctx.send('I do not have permission to change nicknames.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to change the member\'s nickname.')

    @commands.command()
    @commands.has_permissions(manage_nicknames=True)
    async def clearnick(self, ctx, target: str = None, member: discord.Member = None):
        """Clears the mentioned user's nickname or all users' nicknames if 'all' is specified."""

        if target == 'all':
            for member in ctx.guild.members:
                if ctx.guild.me.top_role > member.top_role and ctx.author.top_role > member.top_role:
                    try:
                        await member.edit(nick=None)
                    except discord.Forbidden:
                        pass
                    except discord.HTTPException:
                        pass
            await ctx.send('All nicknames have been cleared.')
        elif member:
            try:
                await member.edit(nick=None)
                await ctx.send(f"{member.mention}'s nickname has been cleared.")
            except discord.Forbidden:
                await ctx.send('I do not have permission to clear nicknames.')
            except discord.HTTPException:
                await ctx.send('An error occurred while trying to clear the member\'s nickname.')
        else:
            await ctx.send('Please mention a member to clear their nickname or use \'all\' to clear all nicknames.')


    @commands.command()
    async def embed(self, ctx, title: str, *, content: str):
        """Sends a message in an embed with supplied title and content."""
        if not title or not content:
            await ctx.send('Please provide both a title and content for the embed.')
            return

        embed = discord.Embed(title=title, description=content, color=discord.Color.blue())

        try:
            await ctx.send(embed=embed)
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to send the embed.')



    @commands.command(aliases=["uinfo"])
    async def userinfo(self, ctx, member: discord.Member = None):
        """Displays information about a user."""
        if member is None:
            member = ctx.author
        user_info_embed = discord.Embed(title=f"{member}", description=f"User information for {member.mention}", color=discord.Color.blue())
        user_info_embed.add_field(name="ID", value=member.id)
        user_info_embed.add_field(name="Joined server", value=member.joined_at.strftime("%Y-%m-%d %H:%M:%S"))
        user_info_embed.add_field(name="Account created", value=member.created_at.strftime("%Y-%m-%d %H:%M:%S"))
        user_info_embed.set_thumbnail(url=member.avatar.url)
        await ctx.send(embed=user_info_embed)

    @commands.command()
    async def serverinfo(self, ctx):
        """Displays information about the server."""
        guild = ctx.guild
        server_info_embed = discord.Embed(title=f"{guild.name}", description="Server information", color=discord.Color.green())
        server_info_embed.add_field(name="ID", value=guild.id)
        server_info_embed.add_field(name="Owner", value=guild.owner.mention)
        server_info_embed.add_field(name="Member count", value=guild.member_count)
        server_info_embed.add_field(name="Role count", value=len(guild.roles))
        server_info_embed.add_field(name="Text Channel count", value=len(guild.text_channels))
        server_info_embed.add_field(name="Voice Channel count", value=len(guild.voice_channels))
        server_info_embed.add_field(name="Created at", value=guild.created_at.strftime("%Y-%m-%d %H:%M:%S"))
        server_info_embed.set_thumbnail(url=guild.icon.url)
        await ctx.send(embed=server_info_embed)

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear(self, ctx, target: str, num_messages: str = None):
        """Clears messages in the current channel. Can clear a specific number of messages, bot messages, or all messages."""

        if target == "bot":
            if num_messages is None:
                num_messages = "all"
        elif num_messages is None:
            num_messages, target = target, None

        num_messages = await self.get_num_messages(ctx, num_messages)
        if num_messages is None:
            return

        try:
            if target == "bot":
                deleted_messages = await ctx.channel.purge(limit=num_messages, check=lambda m: m.author.bot)
            else:
                deleted_messages = await ctx.channel.purge(limit=num_messages)
            await ctx.send(f'{len(deleted_messages)} messages have been cleared.', delete_after=5)
        except discord.Forbidden:
            await ctx.send('I do not have permission to manage messages.')
        except discord.HTTPException:
            await ctx.send('An error occurred while trying to clear messages.')

    async def get_num_messages(self, ctx, num_messages):
        if num_messages == "all":
            return float("inf")
        else:
            try:
                num = int(num_messages)
                if num <= 0:
                    raise ValueError
                return num
            except ValueError:
                await ctx.send("Invalid number of messages provided. Please enter a positive integer or 'all'.")
                return None

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(manage_channels=True)
    async def close(self, ctx):
        """Closes the current channel so no one can send any messages except administrators."""
        overwrite = discord.PermissionOverwrite()
        overwrite.send_messages = False
        admins_role = discord.utils.get(ctx.guild.roles, name="Administrator")
        if admins_role:
            await ctx.channel.set_permissions(admins_role, send_messages=True)
        await ctx.channel.set_permissions(ctx.guild.default_role, overwrite=overwrite)
        await ctx.send("Channel closed. Only administrators can send messages now.")

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(manage_channels=True)
    async def open(self, ctx):
        """Reopens the current channel so everyone can send messages again."""
        overwrite = discord.PermissionOverwrite()
        overwrite.send_messages = True
        await ctx.channel.set_permissions(ctx.guild.default_role, overwrite=overwrite)
        admins_role = discord.utils.get(ctx.guild.roles, name="Administrator")
        if admins_role:
            await ctx.channel.set_permissions(admins_role, send_messages=True)
        await ctx.send("Channel opened. Everyone can send messages now.")

    @userinfo.error
    async def userinfo_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send("Invalid member provided. Please mention a valid member.")

    @close.error
    @open.error
    async def channel_permission_error(self, ctx, error):
        if isinstance(error, commands.BotMissingPermissions):
            await ctx.send("The bot requires 'Manage Channels' permission to perform this action.")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send("You must have Administrator permissions to use this command.")

    @clear.error
    async def clear_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("Missing required argument. Usage: `!clear <bot/all/number> [number]`")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send("The bot requires 'Manage Messages' permission to perform this action.")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send("You must have 'Manage Messages' permission to use this command.")

    @ping.error
    async def ping_error(self, ctx, error):
        if isinstance(error, commands.CommandInvokeError):
            await ctx.send("An error occurred while trying to get the bot's latency.")

    @embed.error
    async def embed_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("Missing title or content. Usage: `!embed <title> <content>`")


    @serverinfo.error
    async def serverinfo_error(self, ctx, error):
        if isinstance(error, commands.CommandInvokeError):
            await ctx.send("An error occurred while trying to get server information.")

async def setup(bot):
    await bot.add_cog(Utility(bot))
