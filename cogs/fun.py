import discord
import os
import random
import requests
from discord.ext import commands
import logging
from dotenv import load_dotenv
from pyowm.owm import OWM

logging.basicConfig(level=logging.INFO)

# Load environment variables
load_dotenv()

owm = OWM(os.environ.get('OPENWEATHERMAP_API_KEY'))


class Fun(commands.Cog):
    """
    A Cog that contains fun commands
    """

    def __init__(self, client):
        self.client = client
        logging.info("Fun Cog initialized")

    @commands.command(name="rolldice")
    async def rolldice(self, ctx):
        """
        Rolls a six-sided die and returns the result
        """
        result = random.randint(1, 6)
        await ctx.send(f"🎲 {ctx.author.mention} rolls a {result}!")

    @commands.command(name="coinflip")
    async def coinflip(self, ctx):
        """
        Flips a coin and returns either heads or tails
        """
        result = random.choice(["heads", "tails"])
        await ctx.send(f"🪙 {ctx.author.mention} flips a coin and gets... {result}!")

    @commands.command(name="catfact")
    async def catfact(self, ctx):
        """
        Returns a random fact about cats
        """
        response = requests.get("https://catfact.ninja/fact")
        fact = response.json()["fact"]
        await ctx.send(f"🐱 Did you know? {fact}")

    @commands.command(name="dog")
    async def dog(self, ctx):
        """
        Returns a random dog image
        """
        response = requests.get("https://dog.ceo/api/breeds/image/random")
        dog_image = response.json()["message"]
        await ctx.send(dog_image)

    @commands.command(name="joke")
    async def joke(self, ctx):
        """
        Returns a random joke
        """
        response = requests.get("https://v2.jokeapi.dev/joke/Any?type=single")
        joke = response.json()["joke"]
        await ctx.send(f"😄 {joke}")

    @commands.command(name="8ball")
    async def _8ball(self, ctx, *, question: str):
        """
        Mimics a Magic 8-Ball, providing a random response to a question
        """
        responses = [
            "It is certain.",
            "It is decidedly so.",
            "Without a doubt.",
            "Yes - definitely.",
            "You may rely on it.",
            "As I see it, yes.",
            "Most likely.",
            "Outlook good.",
            "Yes.",
            "Signs point to yes.",
            "Reply hazy, try again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no.",
            "Outlook not so good.",
            "Very doubtful.",
        ]
        response = random.choice(responses)
        await ctx.send(f"🎱 Question: {question}\nAnswer: {response}")


    @commands.command(name="trivia")
    async def trivia(self, ctx):
        """
        Returns a random trivia question
        """
        response = requests.get("https://opentdb.com/api.php?amount=1&type=multiple")
        question_data = response.json()["results"][0]
        question = question_data["question"]
        correct_answer = question_data["correct_answer"]
        incorrect_answers = question_data["incorrect_answers"]
        all_answers = [correct_answer] + incorrect_answers
        random.shuffle(all_answers)
        answer_list = "\n".join(f"{idx+1}. {answer}" for idx, answer in enumerate(all_answers))
        await ctx.send(f"❓ {question}\n\n{answer_list}")

    @commands.command(name="chucknorris")
    async def chucknorris(self, ctx):
        """
        Returns a random Chuck Norris fact
        """
        response = requests.get("https://api.chucknorris.io/jokes/random")
        fact = response.json()["value"]
        await ctx.send(f"💪 {fact}")

    @commands.command(name="inspire")
    async def inspire(self, ctx):
        """
        Returns a random inspirational quote
        """
        response = requests.get("https://api.quotable.io/random")
        quote = response.json()["content"]
        author = response.json()["author"]
        await ctx.send(f"💭 \"{quote}\" - {author}")

    @commands.command(name="wouldyourather", aliases=["wyr"])
    async def wouldyourather(self, ctx):
        """
        Provides two random 'Would you rather' options to choose from
        """
        response = requests.get("https://www.rrrather.com/botapi")
        option_a = response.json()["choicea"]
        option_b = response.json()["choiceb"]
        await ctx.send(f"Would you rather...\n\n🅰️ {option_a}\n\nor\n\n🅱️ {option_b}")


    @commands.command(name="advice")
    async def advice(self, ctx):
        """
        Returns a random piece of advice
        """
        response = requests.get("https://api.adviceslip.com/advice")
        advice = response.json()["slip"]["advice"]
        await ctx.send(f"💡 {advice}")

    @commands.command(name="pokemon")
    async def pokemon(self, ctx):
        """
        Returns a random Pokémon and its basic information
        """
        response = requests.get("https://pokeapi.co/api/v2/pokemon/{random.randint(1, 898)}")
        pokemon_data = response.json()
        name = pokemon_data["name"].capitalize()
        sprite = pokemon_data["sprites"]["front_default"]
        types = [type_data["type"]["name"] for type_data in pokemon_data["types"]]
        types_str = ", ".join(types)
        await ctx.send(f"🔍 A wild {name} appeared!\nTypes: {types_str}\n{sprite}")


    @commands.command(name="weather")
    async def weather(self, ctx, *, location: str):
        """
        Returns the current weather for a given location
        """
        try:
            observation = owm.weather_manager().weather_at_place(location)
            weather_data = observation.weather
            temp_data = weather_data.temperature("fahrenheit")
            status = weather_data.status
            temp = temp_data["temp"]
            feels_like = temp_data["feels_like"]
            await ctx.send(f"🌡️ The current weather in {location} is {status} with a temperature of {temp}°F (feels like {feels_like}°F).")
        except Exception as e:
            logging.error(f"Weather command error: {e}")  # Add this line to log the error message
            await ctx.send("❌ Unable to fetch weather data. Please check your location and try again.")

async def setup(bot):
    await bot.add_cog(Fun(bot))