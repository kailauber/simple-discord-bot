import discord
from discord.ext import commands
import asyncio
import logging

class Verification(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.verified_role_id = None
        self.verification_manager_role_name = 'Verification Manager'
        self.verification_channel_name = 'verification'
        logging.info("Verification Cog initialized")

    async def process_verification(self, ctx, verification_message):
        # Define a check function to ensure that the reaction is added by the verification manager
        def check_reaction(reaction, user):
            return user.top_role.name == self.verification_manager_role_name and reaction.message.id == verification_message.id

        # Wait for the verification manager to react to the verification message
        try:
            reaction, user = await self.bot.wait_for('reaction_add', check=check_reaction, timeout=60.0)
        except asyncio.TimeoutError:
            await ctx.author.send("Verification has timed out. Please try again later.")
            await verification_message.delete()
            return

        # Add or remove the Verified role depending on the reaction
        if str(reaction.emoji) == "✅":
            await ctx.author.send("Verification has been accepted. Welcome to the server!")
            if ctx.guild:
                verified_role = discord.utils.get(ctx.guild.roles, name='Verified')
                await ctx.author.add_roles(verified_role)
        elif str(reaction.emoji) == "❌":
            await ctx.author.send("Verification has been declined. Please contact the server administrator for more information.")
            if ctx.guild:
                warnings = await self.bot.get_cog("Database").warnings.get_warnings(ctx.author.id)
                reason = "Verification declined"
                user_name = ctx.author.name
                await self.bot.get_cog("Database").warnings.add_warning(ctx.author.id, user_name, reason)

        # Delete the verification message
        await verification_message.delete()

    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def verify(self, ctx):
        """Sends a verification form to the user."""
        verification_form = "Please fill out the following verification form:\nName:\nEmail:\nPhone number:\n"
        await ctx.author.send(verification_form)

        # Define a check function to ensure that the response is a DM from the same user
        def check(message):
            return isinstance(message.channel, discord.DMChannel) and message.author == ctx.author

        # Wait for the user to send the verification form back in DMs
        response = await self.bot.wait_for('message', check=check)

        # Forward the verification form to the verification channel
        verification_channel = discord.utils.get(ctx.guild.channels, name=self.verification_channel_name)
        if not verification_channel:
            verification_channel = await ctx.guild.create_text_channel(self.verification_channel_name)
        embed = discord.Embed(title=f"New verification form from {response.author.name}", description=response.content, color=discord.Color.blue())
        verification_message = await verification_channel.send(embed=embed)

        # Add reaction emojis to the message for the verification manager to accept or decline the verification
        await verification_message.add_reaction("✅")
        await verification_message.add_reaction("❌")

        await self.process_verification(ctx, verification_message)

    @commands.Cog.listener()
    async def on_ready(self):
        await self.add_roles()

    async def add_roles(self):
        # Get the Verified role
        if self.bot.guilds:
            guild = self.bot.guilds[0]
            self.verified_role_id = await self.get_verified_role(guild)
            if not self.verified_role_id:
                verified_role = await guild.create_role(name='Verified')
                self.verified_role_id = verified_role.id

    async def get_verified_role(self, guild):
        verified_role = discord.utils.get(guild.roles, name='Verified')
        if not verified_role:
            return None
        return verified_role.id

    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def verifyinteract(self, ctx):
        """Interactive verification command."""
        def check(message):
            return message.channel == ctx.channel and message.author == ctx.author

        # Ask the user if they are a buyer or a seller
        await ctx.author.send("Are you a buyer or a seller? Type `cancel` to cancel the process.")
        buyer_or_seller = await self.bot.wait_for('message', check=check)
        if buyer_or_seller.content.lower() == "cancel":
            await ctx.author.send("Verification process cancelled.")
            return

        # Ask additional questions based on the user's response
        if buyer_or_seller.content.lower() == "buyer":
            await ctx.author.send("Please provide your email address:")
            email = await self.bot.wait_for('message', check=check)
            if email.content.lower() == "cancel":
                await ctx.author.send("Verification process cancelled.")
                return

            await ctx.author.send("Please provide your phone number:")
            phone = await self.bot.wait_for('message', check=check)
            if phone.content.lower() == "cancel":
                await ctx.author.send("Verification process cancelled.")
                return

            response_content = f"Buyer\nEmail: {email.content}\nPhone number: {phone.content}"
        elif buyer_or_seller.content.lower() == "seller":
            await ctx.author.send("Please provide your store name:")
            store_name = await self.bot.wait_for('message', check=check)
            if store_name.content.lower() == "cancel":
                await ctx.author.send("Verification process cancelled.")
                return

            await ctx.author.send("Please provide your store website:")
            website = await self.bot.wait_for('message', check=check)
            if website.content.lower() == "cancel":
                await ctx.author.send("Verification process cancelled.")
                return

            response_content = f"Seller\nStore Name: {store_name.content}\nWebsite: {website.content}"
        else:
            await ctx.author.send("Invalid response. Please use the command again and enter either 'buyer' or 'seller'.")
            return

        # Forward the verification form to the verification channel
        verification_channel = discord.utils.get(ctx.guild.channels, name=self.verification_channel_name)
        if not verification_channel:
            verification_channel = await ctx.guild.create_text_channel(self.verification_channel_name)
        embed = discord.Embed(title=f"New interactive verification form from {ctx.author.name}", description=response_content, color=discord.Color.blue())
        verification_message = await verification_channel.send(embed=embed)

        # Add reaction emojis to the message for the verification manager to accept or decline the verification
        await verification_message.add_reaction("✅")
        await verification_message.add_reaction("❌")

        await self.process_verification(ctx, verification_message)

    @commands.command()
    @commands.has_role('Verification Manager')
    async def manualverify(self, ctx, member: discord.Member):
        """Manually verify a user."""
        verified_role = discord.utils.get(ctx.guild.roles, name='Verified')
        await member.add_roles(verified_role)
        await ctx.send(f"{member.mention} has been manually verified by {ctx.author.mention}.")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def setverificationchannel(self, ctx, channel: discord.TextChannel):
        """Set the verification channel."""
        self.verification_channel_name = channel.name
        await ctx.send(f"Verification channel has been set to {channel.mention}.")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def setverificationmanagerrole(self, ctx, role: discord.Role):
        """Set the Verification Manager role."""
        self.verification_manager_role_name = role.name
        await ctx.send(f"Verification Manager role has been set to {role.mention}.")

async def setup(bot):
    await bot.add_cog(Verification(bot))
