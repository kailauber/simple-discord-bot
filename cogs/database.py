import discord
import os
import logging
from discord.ext import commands
from dotenv import load_dotenv
from motor.motor_asyncio import AsyncIOMotorClient

# Load environment variables
load_dotenv()


class Warnings:
    def __init__(self, collection):
        self.collection = collection

    async def add_warning(self, user_id, user_name, reason):
        """Adds a warning for a user."""
        await self.collection.update_one(
            {"_id": user_id},
            {"$push": {"warnings": {"reason": reason, "user_name": user_name}}},
            upsert=True
        )

    async def get_warnings(self, user_id):
        """Retrieves all warnings for a user."""
        result = await self.collection.find_one({"_id": user_id})
        if result:
            return [warning["reason"] for warning in result["warnings"]]
        else:
            return []

    async def clear_warnings(self, user_id):
        """Clears all warnings for a user."""
        await self.collection.update_one(
            {"_id": user_id},
            {"$set": {"warnings": []}}
        )

    async def update_warnings(self, user_id, warnings):
        """Updates the warnings for a user."""
        await self.collection.update_one(
            {"_id": user_id},
            {"$set": {"warnings": [{"reason": warning, "user_name": warnings[0]["user_name"]} for warning in warnings]}}
        )


class Database(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.mongo_client = AsyncIOMotorClient(os.getenv('CONNECTION_STRING'))
        self.db = self.mongo_client['SimpleBot']
        self.warnings_collection = self.db['warnings']
        self.warnings = Warnings(self.warnings_collection)
        self.guild_settings = self.db['guild_settings']
        logging.info("Database Cog initialized")

    async def set_welcome_channel(self, guild_id, channel_id):
        await self.guild_settings.update_one(
            {"_id": guild_id},
            {"$set": {"welcome_channel": channel_id}},
            upsert=True
        )

    async def set_welcome_message(self, guild_id, message):
        await self.guild_settings.update_one(
            {"_id": guild_id},
            {"$set": {"welcome_message": message}},
            upsert=True
        )

    async def set_goodbye_channel(self, guild_id, channel_id):
        await self.guild_settings.update_one(
            {"_id": guild_id},
            {"$set": {"goodbye_channel": channel_id}},
            upsert=True
        )

    async def set_goodbye_message(self, guild_id, message):
        await self.guild_settings.update_one(
            {"_id": guild_id},
            {"$set": {"goodbye_message": message}},
            upsert=True
        )

    # Add methods for retrieving the values if needed
    async def get_welcome_channel(self, guild_id):
        result = await self.guild_settings.find_one({"_id": guild_id})
        return result.get("welcome_channel") if result else None

    async def get_welcome_message(self, guild_id):
        result = await self.guild_settings.find_one({"_id": guild_id})
        return result.get("welcome_message") if result else None

    async def get_goodbye_channel(self, guild_id):
        result = await self.guild_settings.find_one({"_id": guild_id})
        return result.get("goodbye_channel") if result else None

    async def get_goodbye_message(self, guild_id):
        result = await self.guild_settings.find_one({"_id": guild_id})
        return result.get("goodbye_message") if result else None

    async def set_prefix(self, guild_id, prefix):
        await self.guild_settings.update_one(
            {"_id": guild_id},
            {"$set": {"prefix": prefix}},
            upsert=True
        )

    async def get_prefix(self, guild_id):
        result = await self.guild_settings.find_one({"_id": guild_id})
        return result.get("prefix") if result else '!'


async def setup(bot):
    await bot.add_cog(Database(bot))