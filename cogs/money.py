import discord
from discord.ext import commands
import logging
from dotenv import load_dotenv
import requests
import os
import asyncio

# Load environment variables
load_dotenv()

logging.basicConfig(level=logging.INFO)

class Money(commands.Cog):
    """
    A Cog that provides money commands for the bot.
    """

    def __init__(self, client):
        self.client = client
        self.alphavantage_api_key = os.getenv("YOUR_ALPHA_VANTAGE_API_KEY")
        self.finnhub_api_key = os.getenv("YOUR_FINNHUB_API_KEY")
        self.alerts = {}
        logging.info("Money Cog initialized")

    @commands.command()
    async def price(self, ctx, *, asset: str):
        """
        Get the current price of the asset
        """
        try:
            # Construct the request URL
            url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={asset}&apikey={self.alphavantage_api_key}'

            # Attempt to get the response from the Alpha Vantage API
            response = requests.get(url).json()

            # Check if the response contains the 'Global Quote' field
            if 'Global Quote' not in response:
                logging.error(f"Unexpected API response: {response}")
                await ctx.send(f"Failed to fetch the price of {asset}. The API response does not contain expected data.")
                return

            # Check if the 'Global Quote' field contains the '05. price' field
            if '05. price' not in response['Global Quote']:
                logging.error(f"Unexpected 'Global Quote' format: {response['Global Quote']}")
                await ctx.send(f"Failed to fetch the price of {asset}. The 'Global Quote' data does not contain the expected '05. price' field.")
                return

            # Retrieve the price and send it
            price = response['Global Quote']['05. price']
            await ctx.send(f'The current price of {asset} is {price}')

        except requests.exceptions.RequestException as e:
            # Handle possible request errors
            logging.error(f"Request to Alpha Vantage API failed: {e}")
            await ctx.send(f"Failed to fetch the price of {asset}. Could not reach the API.")

        except Exception as e:
            # Handle any other unexpected exceptions
            logging.error(f"An error occurred when trying to fetch the price of {asset}: {e}")
            await ctx.send(f"Failed to fetch the price of {asset}. Please try again later.")

    @commands.command()
    async def crypto_price(self, ctx, *, symbol: str):
        """
        Get the current price of the cryptocurrency
        """
        try:
            url = f'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency={symbol}&to_currency=USD&apikey={self.alphavantage_api_key}'
            response = requests.get(url).json()
            price = response['Realtime Currency Exchange Rate']['5. Exchange Rate']
            await ctx.send(f'The current price of {symbol} is {price} USD')
        except Exception as e:
            logging.error(f"An error occurred when trying to fetch the price of {symbol}: {e}")
            await ctx.send(f"Failed to fetch the price of {symbol}. Please try again later.")


    @commands.command()
    async def company_search(self, ctx, *, keywords: str):
        """
        Search for companies using keywords
        """
        try:
            url = f'https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords={keywords}&apikey={self.alphavantage_api_key}'
            response = requests.get(url).json()
            matches = response['bestMatches']
            if matches:
                output = '\n'.join([f"{match['1. symbol']}: {match['2. name']} ({match['3. type']}, {match['4. region']})" for match in matches])
                await ctx.send(f"Here are the companies matching your search:\n{output}")
            else:
                await ctx.send("No companies found with your keywords.")
        except Exception as e:
            logging.error(f"An error occurred when trying to search companies: {e}")
            await ctx.send(f"Failed to search companies. Please try again later.")

    @commands.command()
    async def sma(self, ctx, *, symbol: str):
        """
        Get the simple moving average (SMA) of a stock
        """
        try:
            url = f'https://www.alphavantage.co/query?function=SMA&symbol={symbol}&interval=daily&time_period=10&series_type=close&apikey={self.alphavantage_api_key}'
            response = requests.get(url).json()
            sma = response['Technical Analysis: SMA']
            output = '\n'.join([f"{date}: {data['SMA']}" for date, data in sma.items()])
            await ctx.send(f"Here is the SMA for {symbol}:\n{output}")
        except Exception as e:
            logging.error(f"An error occurred when trying to fetch the SMA of {symbol}: {e}")
            await ctx.send(f"Failed to fetch the SMA of {symbol}. Please try again later.")


    @commands.command()
    async def set_alert(self, ctx, asset: str, price: float):
        """
        Set an alert for the asset when it reaches the given price
        """
        self.alerts[asset] = price
        await ctx.send(f'Set alert for {asset} at {price}')

    @commands.command()
    async def remove_alert(self, ctx, asset: str):
        """
        Remove an alert for an asset
        """
        if asset in self.alerts:
            del self.alerts[asset]
            await ctx.send(f'Removed alert for {asset}')
        else:
            await ctx.send(f'No alert set for {asset}')

    async def check_alerts(self):
        while True:
            for asset, target_price in list(self.alerts.items()): # create a copy of the dictionary
                try:
                    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={asset}&apikey={self.alphavantage_api_key}'
                    response = requests.get(url).json()
                    current_price = float(response['Global Quote']['05. price'])
                    if current_price >= target_price:
                        await self.client.say(f'{asset} has reached the target price {target_price}')
                        del self.alerts[asset]
                except Exception as e:
                    logging.error(f"An error occurred when checking alerts for {asset}: {e}")
            await asyncio.sleep(60)  # check every minute

    @commands.Cog.listener()
    async def on_ready(self):
        self.client.loop.create_task(self.check_alerts())


async def setup(bot):
    await bot.add_cog(Money(bot))
