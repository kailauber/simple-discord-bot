import discord
from discord.ext import commands
import logging

class Greeting(commands.Cog):
    """A Cog to handle welcome and goodbye messages for a Discord server."""

    def __init__(self, client):
        self.client = client
        self.db = None
        client.loop.create_task(self.initialize_database())

    async def initialize_database(self):
        """Initialize the database connection."""
        await self.client.wait_until_ready()
        self.db = self.client.get_cog('Database')
        logging.info("Greeting Cog initialized")

    async def cog_check(self, ctx):
        """Check if the Database Cog is loaded."""
        if self.db is None:
            await ctx.send("Database cog is not loaded.")
            return False
        return True

    async def send_instructions(self, ctx, cmd):
        """Send usage instructions for the specified command."""
        embed = discord.Embed(
            title=f"{cmd.capitalize()} Command Instructions",
            description=(
                f"Usage:\n"
                f"`{cmd} channel <#channel>` - Set the {cmd} channel\n"
                f"`{cmd} message <message>` - Set the {cmd} message"
            ),
            color=discord.Color.blue()
        )
        await ctx.send(embed=embed)


    @commands.group(invoke_without_command=True)
    async def welcome(self, ctx):
        """Handle the welcome command."""
        await self.send_instructions(ctx, "welcome")

    @welcome.command()
    async def channel(self, ctx, channel: discord.TextChannel):
        """Set the welcome channel."""
        await self.db.set_welcome_channel(ctx.guild.id, channel.id)
        await ctx.send(f"Welcome channel has been set to {channel.mention}")

    @welcome.command()
    async def message(self, ctx, *, message: str):
        """Set the welcome message."""
        await self.db.set_welcome_message(ctx.guild.id, message)
        await ctx.send(f"Welcome message has been set to:\n{message}")

    @commands.group(invoke_without_command=True)
    async def goodbye(self, ctx):
        """Handle the goodbye command."""
        await self.send_instructions(ctx, "goodbye")

    @goodbye.command()
    async def channel(self, ctx, channel: discord.TextChannel):
        """Set the goodbye channel."""
        await self.db.set_goodbye_channel(ctx.guild.id, channel.id)
        await ctx.send(f"Goodbye channel has been set to {channel.mention}")

    @goodbye.command()
    async def message(self, ctx, *, message: str):
        """Set the goodbye message."""
        await self.db.set_goodbye_message(ctx.guild.id, message)
        await ctx.send(f"Goodbye message has been set to:\n{message}")

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        """Handle command errors and provide specific error messages and instructions."""
        if isinstance(error, commands.BadArgument):
            await ctx.send("Invalid argument. Please make sure you've entered the correct information.")
        elif isinstance(error, commands.MissingRequiredArgument):
            if ctx.command and ctx.command.parent:  # Add this line to check if ctx.command and ctx.command.parent are not None
                if ctx.command.parent.name == "welcome":
                    await self.send_instructions(ctx, "welcome")
                elif ctx.command.parent.name == "goodbye":
                    await self.send_instructions(ctx, "goodbye")


async def setup(client):
    """Add the WelcomeGoodbye Cog to the client."""
    await client.add_cog(Greeting(client))
